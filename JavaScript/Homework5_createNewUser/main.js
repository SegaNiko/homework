
function formatDate(value) {
    let date = new Date(value);
    let dd = date.getDate();
    if (dd < 10) dd = '0' + dd;

    let mm = date.getMonth() + 1;
    if (mm < 10) mm = '0' + mm;

    let yy = date.getFullYear() % 100;
    if (yy < 10) yy = '0' + yy;

    let bd = dd + '.' + mm + '.' + yy;
    return bd;
}
function getAge(date){
    let minus = new Date(date).getFullYear();
    let age = new Date().getFullYear() - minus;
    return console.log(age);
}
function getLogin(firstName,lastName){
    let newLogin = firstName.charAt(0).toLowerCase() + lastName.toLowerCase();
    return console.log(`Your login is: ${newLogin}`);
}
function getPassword(firstName,lastName,year){
    let newPassword = firstName.charAt(0).toUpperCase() + lastName.toLowerCase() + year;
    return console.log(`Your Password is: ${newPassword}`);
}

function CreateNewUser() {
    this.firstName = prompt('Enter you name: ','');
    while (this.firstName === ''|| +this.firstName === +this.firstName){
        this.firstName = prompt('Enter you name AGAIN: ','');
    }
    this.lastName = prompt('Enter you second name','');
    while (this.lastName === ''|| +this.lastName === +this.lastName){
        this.lastName = prompt('Enter you second name AGAIN: ','');
    }
    this.birthday = prompt('Enter you birthday: ','mm.dd.yyyy');
    this.birthday = formatDate(this.birthday);
    let yearOld = this.year();
    formatDate(this.birthday);
    getLogin(this.firstName,this.lastName);
    getAge(this.birthday);
    getPassword(this.firstName,this.lastName,yearOld);

}

CreateNewUser.prototype.year = function(){
    return new Date(this.birthday).getFullYear();
};

let newUser = new CreateNewUser();
console.log(newUser);
