

function isPrime(num) {
  let i = 2;

  for(; i < num; i++)
    if(num % i === 0) return false;
  return num > 1;
}

function filter(){
	let m = prompt("Введитe число m");
    let n = prompt("Введитe число n");

    while (!Number.isInteger(+m) && !Number.isInteger(+n)) {
        console.log('Ошибка. Введите числа заново!')
        m = prompt("Введитe число m");
        n = prompt("Введитe число n");
    }

    if (m > n) {
        console.log('Ошибка.m должно быть меньше чем n')
        m = prompt("Введитe число m");
        n = prompt("Введитe число n");
    }

    let i = 1;

	for (; i <= n - m ; i++) {
        if (isPrime(i)) {
            console.log(i)
        }   
    }
		
}

filter();