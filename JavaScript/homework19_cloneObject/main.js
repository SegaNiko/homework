//
// Технические требования:
//     Написать функцию для рекурсивного полного клонирования объекта
//     (без единой передачи по ссылке, внутренняя вложенность свойств объекта может быть достаточно большой).
//     Функция должна успешно копировать свойства в виде объектов и массивов на любом уровне вложенности.
//     В коде нельзя использовать встроенные механизмы клонирования, такие как функция Object.assign() или спред-оператор.
let student = {
    name: "dima",
    lastname: "Vinich",
    scores: {
        math: 33,
        paint:55,
        fiziology:45
    }
};

//     Реализация с помощю ES5
//           |
// let cloned = JSON.parse(JSON.stringify(student));

//     Реализация с помощю ES6
//           |
// let cloneObject = Object.assign({}, student);

//     Реализация этого способа приметивна, и копирует только свойства объекта но не методы прописание вручную
//           |
// function cloneObject(arr) {
//     let copyObject = [];
//     let key;
//
//     for (key in arr) {
//         copyObject[key] = arr[key];
//     }
//
//     return copyObject;
// }

//     Реализация копирование объекта рекурсивным способом
//              |
const Z = {
    a: 5,
    b: { g: 8, y: 9, t: { q: 48 } },
    x: 47,
    l: { f: 85, p: { u: 89, m: 7 }, s: 71 },
    r: { h: 9, a: 'test', s: 'test2' }
};
const Y = deepClone(Z);

function deepClone(obj) {
    const clObj = {};
    for(const i in obj) {
        if (obj[i] instanceof Object) {
            clObj[i] = deepClone(obj[i]);
            continue;
        }
        clObj[i] = obj[i];
    }
    return clObj;
}
Y.x = 76;
console.log(Y);
console.log(Z);

