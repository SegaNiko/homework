// Технические требования:
// Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
// При вызове функция должна спросить у вызывающего имя и фамилию.
// Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
// Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя,
// соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
// Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin(). 
//Вывести в консоль результат выполнения функции.


function CreateNewUser() {
	this.firstName = prompt('Enter you name: ','');
	console.log(typeof this.firstName);
	while (this.firstName === ''|| +this.firstName === +this.firstName){
		this.firstName = prompt('Enter you name AGAIN: ','');
	}

	this.lastName = prompt('Enter you second name','');
	while (this.lastName === ''|| +this.lastName === +this.lastName){
		this.lastName = prompt('Enter you second name AGAIN: ','');
	}

	this.getLogin = function(){
		let newLogin = this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
		return console.log(`Your login is: ${newLogin}`);
	}
}

let newUser = new CreateNewUser();


