function tab() {
     const tabTitle = document.querySelectorAll('.tabs-title'),
           tabContent = document.querySelectorAll('.tab');
     let   tabName;
    tabTitle.forEach(item =>{
        item.addEventListener('click',selectTabTitle)
    });

    function selectTabTitle() {
         tabTitle.forEach(item =>{
             item.classList.remove('active');
         });
        this.classList.add('active');
        tabName = this.getAttribute('data-tab-name');
        selectTabContent(tabName);
    }
    
    function selectTabContent(tabName) {
        tabContent.forEach(item => {
            item.classList.remove('active')
        });
        const elm = document.getElementsByClassName(tabName)[0];
        elm.classList.add('active');
    }
}

tab();