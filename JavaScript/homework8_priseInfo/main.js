let pol = document.querySelector(".input");
let prise = document.createElement("span");
prise.className = "span";
document.querySelector(".main").prepend(prise);
pol.addEventListener("blur", addLine);

function addLine() {
  if (!Number.isInteger(+pol.value)) {
    pol.className = "input red";
    prise.innerHTML = `Enter prise again`;
    return addLine();
  }
  if (pol.className === "input red") {
    pol.className = "input";
  }
  prise.innerHTML = `$ ${pol.value}`;
}

function onBlur() {}
//
//
//
// let text = document.querySelector('.input');
// prise = document.querySelector("span").innerHTML = `$ ${text}`;
