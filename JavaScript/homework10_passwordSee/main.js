function passwordSee() {
    let input = document.querySelectorAll('input'),
        icons = document.querySelectorAll("i"),
        dataIcon;

    icons.forEach(item => {
        item.addEventListener('click', toggle)
    });

    function toggle() {
        if (this.classList === "fas fa-eye icon-password") {
            this.classList = "fas fa-eye-slash icon-password";
            dataIcon = this.getAttribute("data-name");
            changeText(dataIcon);
            return
        }
        if (this.classList === "fas fa-eye-slash icon-password") {
            this.classList = "fas fa-eye icon-password";
            dataIcon = this.getAttribute("data-name");
            changeText(dataIcon);
        }
    }

    function changeText(cls) {
        input.forEach(item => {
            if (item.classList.contains(cls)) {
                if (item.type === "password") {
                    item.type = "text";
                } else {
                    item.type = "password";
                }
            }
        })
    }
}

passwordSee();
