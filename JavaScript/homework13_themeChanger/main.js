let themeNumber = 1,
  defaultTheme = localStorage.getItem("lastTheme");
exit = "css";

function getHref(num) {
  return exit + "/" + "style-" + num + ".css";
}

function getThemeNumber() {
  if (themeNumber % 2 === 1) {
    themeNumber = 2;
  } else {
    themeNumber = 1;
  }
  return themeNumber;
}

function initialize() {
  let link = document.createElement("link");
  link.setAttribute("rel", "stylesheet");
  link.setAttribute("type", "text/css");
  link.setAttribute("href", getHref(defaultTheme));
  link.setAttribute("id", "theme");

  document.head.appendChild(link);
}

function initListener() {
  document.querySelector(".theme").addEventListener("click", changeTheme);
  localStorage.setItem("lastTheme", themeNumber);
}

function changeTheme() {
  if (document.getElementById("theme")) {
    document
      .getElementById("theme")
      .setAttribute("href", getHref(getThemeNumber()));
    return;
  }

  const linkTheme = document.createElement("link");
  linkTheme.setAttribute("rel", "stylesheet");
  linkTheme.setAttribute("href", getHref(getThemeNumber()));
  linkTheme.setAttribute("type", "text/css");
  linkTheme.setAttribute("id", "theme");

  document.querySelector("head").append(linkTheme);

  localStorage.setItem("lastTheme", themeNumber);
}

initialize();
window.addEventListener("load", initListener);
