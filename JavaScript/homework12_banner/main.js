function showImg() {
  let imgs = document.querySelectorAll(".image-to-show"),
    btn = document.createElement("a"),
    interval = 1000,
    i = 0;
  btn.className = "btn";
  btn.innerHTML = "Остановить на этой картинке";
  document.querySelector("body").after(btn);

  btn.addEventListener("click", stop);

  const intervalID = setInterval(() => {
    imgs.forEach((item, index) => {
      if (item.classList.contains("active")) {
        item.classList.remove("active");
      }
    });

    i++;

    if (i === imgs.length) {
      i = 0;
    }
    imgs[i].classList.add("active");
  }, interval);

  function stop() {
    clearInterval(intervalID);
  }
}
showImg();
