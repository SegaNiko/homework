const path = require("path");
module.export = {
  entry: "",
  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "public"),
  },
  devServer: {
    port: 3000,
  },
};
