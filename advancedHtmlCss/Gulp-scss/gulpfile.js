const gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    //watch = require('gulp-watch'),
    sass = require('gulp-sass'),
    cssmin = require('gulp-minify-css'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    prefixer = require('gulp-autoprefixer'),
    clean = require('gulp-clean'),
    browserSync = require("browser-sync").create(),
    reload = browserSync.reload;

const config = {
    server: {
        baseDir: './'
    },
    port: 3000,
};

const path = {
    build: { //Тут мы укажем куда складывать готовые после сборки файлы
        html: 'dist/',
        js: 'dist/js/',
        css: 'dist/css/',
        img: 'dist/img/',
        fonts: 'dist/fonts/'
    },
    src: { //Пути откуда брать исходники
        html: 'index.html',
        js: 'src/js/**/*.js',
        css: 'src/sass/**/*.scss',
        img: 'src/img/**/*.*', //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
        fonts: 'src/fonts/**/*.*'
    },
    watch: { //Путь для отслежуванния файлов сборки
        html: 'index.html',
        js: 'src/js/**/*.js',
        css: 'src/sass/**/*.scss',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    clean: 'dist/'
};

gulp.task('clean',function (){
    return gulp.src(path.clean)
        .pipe(clean());
});


gulp.task('browser-sync', function() {
    browserSync.init(config);
});

gulp.task('htmlBuild', function (done) {
    gulp.src(path.src.html) //Выберем файлы по нужному пути
        .pipe(reload({stream: true})); //И перезагрузим наш сервер для обновлений
        done();
});

gulp.task('jsBuild', function () {
    return gulp.src(path.src.js) //Найдем наш main файл
        //.pipe(uglify()) //Сожмем наш js
        .pipe(gulp.dest(path.build.js)) //Выплюнем готовый файл в build
        .pipe(reload({stream: true})); //И перезагрузим сервер
});

gulp.task('cssBuild', function () {
    return gulp.src(path.src.css) //Выберем наш main.scss
        .pipe(sass()) //Скомпилируеm
       // .pipe(prefixer()) //Добавим вендорные префиксы
      //  .pipe(cssmin()) //Сожимаем
        .pipe(gulp.dest(path.build.css)) //И в dist
        .pipe(reload({stream: true}));
});

gulp.task('imageBuild', function () {
    return gulp.src(path.src.img) //Выберем наши картинки
        .pipe(imagemin({ //Сожмем их
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.img)) //И бросим в build
        .pipe(reload({stream: true}));
});

gulp.task('fontsBuild', function() {
    return gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

gulp.task('watchFiles', function(){
     gulp.watch(path.watch.html, gulp.series('htmlBuild'));
     gulp.watch(path.watch.css, gulp.parallel('cssBuild'));
     gulp.watch(path.watch.js, gulp.parallel('jsBuild'));
     gulp.watch(path.watch.img, gulp.parallel('imageBuild'));
     gulp.watch(path.watch.fonts, gulp.parallel('fontsBuild'));
});

const build = gulp.parallel('htmlBuild','cssBuild', 'jsBuild', 'imageBuild', 'fontsBuild');
const watch = gulp.parallel( 'browser-sync','watchFiles');
gulp.task('build',build);
gulp.task('dev',gulp.parallel('cssBuild', 'jsBuild', 'imageBuild', 'fontsBuild'));

gulp.task('default',watch);
