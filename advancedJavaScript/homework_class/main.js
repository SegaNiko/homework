class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
}

class Programmer extends  Employee  {
  constructor (name, age, salary ,lang) {
    super(name, age, salary)
    this.salary = salary * 3;
    this.lang = lang
  }  
}

const user = new Employee("user",25,2555);
const user2 = new Programmer("Ivan", 100, 2555, ["ochen'",'mnogo',"yazikov"]);
const user3 = new Programmer("Sergii", 21, 5774, ["JS",'React',"TypeScript"]);
const user4 = new Programmer("Shao Lin'", 36, 13497, ["Go",'GQL',"PHP"]);

console.log("user  :",user)
console.log("user2 :",user2)
console.log("user3 :",user3)
console.log("user4 :",user4)



