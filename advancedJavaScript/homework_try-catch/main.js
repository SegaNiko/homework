const books = [
  { 
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70 
  }, 
  {
   author: "Скотт Бэккер",
   name: "Воин-пророк",
  }, 
  { 
    name: "Тысячекратная мысль",
    price: 70
  }, 
  { 
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  }, 
  {
   author: "Дарья Донцова",
   name: "Детектив на диете",
   price: 40
  },
  {
   author: "Дарья Донцова",
   name: "Дед Снегур и Морозочка",
  }
];

const div = document.getElementById('root');
const ul = document.createElement("ul");
div.append(ul)

books.map(item =>{
  try {
    if (item.author && item.name && item.price){
       const li = document.createElement('li')
       li.innerHTML = `Author : ${item.author}, Book name : "${item.name}", Price : $${item.price}`
       ul.append(li)
    }if (!item.author) {
      throw new AuthorError("Not foung author")
    }
    if (!item.name) {
      throw new NameError("Not foung author")
    }
    if (!item.price) {
      throw new PriceError("Not foung author")
    }
    
  }catch(err) {
    console.log(err.message)
  }
})