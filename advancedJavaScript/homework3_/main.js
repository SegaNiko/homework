const requestURL = "https://swapi.dev/api/films";

function sendRequest(method, url) {
  return new Promise ((resolve,reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open(method, url);
    xhr.responseType = 'json';

    xhr.onload = () => {
      if (xhr.status >= 400){
        reject(xhr.response)
      } else {
        const arr = (xhr.response);
        resolve(arr)
      }
    };

    xhr.onerror = () => {
      reject(xhr.response)
    };

    xhr.send();
  });


}

sendRequest('GET', requestURL)
    .then(data => console.log(data))
    .catch(err => console.log(err));



//   xhr.onload = () => {
//     const films = xhr.response.results;
//
//     films.forEach(item => {
//       const div = document.createElement('div');
//       div.className = 'title__wrap';
//       document.querySelector(".container").prepend(div);
//
//       const title = document.createElement('h1');
//       title.className = 'title__name';
//       title.innerHTML = `Названия фильма :${item.title}`;
//       document.querySelector(`.title__wrap`).append(title);
//
//       const characters = item.characters;
//       console.log(characters);
//       characters.forEach(item  => {
//         xhr.open('GET', item);
//         xhr.responseType = 'json';
//         console.log(xhr.response);
//         xhr.onload = () =>{
//           const actor = xhr.response.results;
//           console.log(actor)
//         };
//         xhr.send();
//       })
//       // document.createElement('p');
//       // characters.className = 'title__actors';
//       // characters.innerHTML = `Актёрсkий состав :${item.characters}`;
//       // document.querySelector(`.title__wrap`).append(characters);
//     });
//   };
//   xhr.onerror = () => {
//     if (xhr.status >= 400){
//       console.log(`Error ${xhr.error}`)
//     } else {
//       console.log(xhr.response);
//     }
//   };
//   xhr.send();
// });
