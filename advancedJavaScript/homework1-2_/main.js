const SIZE_SMALL = [50, 20],
    SIZE_LARGE = [100, 30],
    STUFFING_CHEESE = [10, 20],
    STUFFING_SALAD = [20, 5],
    STUFFING_POTATO = [15, 10],
    TOPPING_MAYO = [15, 0],
    TOPPING_SPICE = [20, 5];

class Hamburger {
    constructor(size, stuffing, topping = null) {
        this.size = size;
        this.stuffing = stuffing;
        this.topping = topping;
        this.topping2 = null;
    }

    addTopping(topping,topping2) {
        this.topping = topping;
        this.topping2 = topping2;
    }

    removeTopping(topping) {
        this.topping = null;
    }

    calcPrise() {
        let prise = this.size[0] + this.stuffing[0] + this.topping[0];
        return `Вес бургера ${prise}грамов`
    }

    calcCalories() {
        const calories = this.size[1] + this.stuffing[1] + this.topping[1];
        return `Калории бургера бургера ${calories}калл`;
    }

    getSize() {
        if (this.size === SIZE_SMALL) {
            return console.log("Small size")
        } else {
            return console.log("Small large")
        }
    };

    getStuffing() {
        if (this.stuffing === STUFFING_CHEESE) {
            return console.log("Stuffing cheese");
        } if (this.stuffing === STUFFING_SALAD) {
            return console.log("Stuffing salad");
        } else {
            return console.log("Stuffing potato");
        }
    };

    getTopping() {
        if (this.topping != null){
            if ( this.topping === TOPPING_MAYO ){
                return console.log("Topping with mayo")
            } else {
                return console.log("Topping with spice")
            }
        } else {
            return console.log("Without topping");
        }
    };

}

class HamburgerTopping extends Hamburger {
    constructor(size, stuffing, topping) {
        super(size, stuffing);

        this.topping = topping;
    }
}


// // function Hamburger(size, stuffing) {
// //
// //
// // }
//
//
// //  * Добавить добавку к гамбургеру. Можно добавить несколько
// //  * добавок, при условии, что они разные.
// //  *
// //  * @param topping     Тип добавки
// //  * @throws {HamburgerException}  При неправильном использовании
// Hamburger.prototype.addTopping = function (topping) {
//
// };
//
// //  Получить список добавок.
// //  @return {Array} Массив добавленных добавок, содержит константы
// //  Hamburger.TOPPING_*
// Hamburger.prototype.getToppings = function () {
//
// };
//
// //  * Узнать размер гамбургера
// Hamburger.prototype.getSize = function () {
//
// };
//
// //  * Узнать начинку гамбургера
// Hamburger.prototype.getStuffing = function () {
//
// };
//
// //   Убрать топинг
// Hamburger.prototype.removeTopping = function (topping) {
//
// };
//
// //  * Узнать цену гамбургера
// //  * @return {Number} Цена в тугриках
// Hamburger.prototype.calculatePrice = function () {
//
// };
//
// //  * Узнать калорийность
// //  * @return {Number} Калорийность в калориях
// Hamburger.prototype.calculateCalories = function () {
//
// };
//
// // * Представляет информацию об ошибке в ходе работы с гамбургером.
// //  * Подробности хранятся в свойстве message.
// //  * @constructor
// //  */
// function HamburgerException() {
//
// }
//
// // /* Размеры, виды начинок и добавок */
//
// let burger = new Hamburger(SIZE_SMALL,STUFFING_CHEESE,TOPPING_MAYO);
//
// console.log(burger.calcCalories());
// console.log(burger.calcPrise());
// console.log(burger);
//
