const div = document.getElementById('root');
const wrap = document.createElement("div");
wrap.style.margin = "0 auto"
wrap.style.width = "800px"
div.append(wrap)
const url = 'https://swapi.dev/api/films/';

const getCharacters = async (arr,id) => {
  await arr.map(async item => {
    const p2 = document.createElement('span');
    let res = await fetch(item).then( res => res.json())
    p2.innerHTML = `${res.name}, `
    document.querySelector(`.gg${id}`).append(p2)
  })
}
const makeGetRequest = async () => {
  let response = await fetch(url).then(res => res.json()).then(data => data.results)
  response.sort((a, b) => a.episode_id - b.episode_id).map(async item =>{
    const h4 = document.createElement('h4')
    const p = document.createElement('p')
    const div2 = document.createElement('div')
    div2.classList.add(`gg${item.episode_id}`)
    h4.innerHTML = ` Episode: ${item.episode_id} Title : "${item.title}"`
    p.innerHTML = `Opening : ${item.opening_crawl}`
    
    wrap.append(h4)
    wrap.append(p)
    wrap.append(div2)
    await getCharacters(item.characters, item.episode_id)
  })

  return response
}

makeGetRequest()