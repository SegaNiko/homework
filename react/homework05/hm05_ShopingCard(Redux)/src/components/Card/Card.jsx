import React from "react";
import { useDispatch } from "react-redux";
import action from "../../reducers/actions";

import "./index.scss";

const items = [
  {
    id: 1,
    name: "Banana",
    price: 0.5,
    src:
      "http://images.all-free-download.com/images/graphicthumb/vector_illustration_of_ripe_bananas_567893.jpg",
    alt: "It is Banana",
    count: 1,
  },
  {
    id: 2,
    name: "Orange",
    price: 1.5,
    src: "http://www.azspagirls.com/files/2010/09/orange.jpg",
    alt: "It is Orange",
    count: 1,
  },
  {
    id: 3,
    name: "Lemon",
    price: 5,
    src: "https://3.imimg.com/data3/IC/JO/MY-9839190/organic-lemon-250x250.jpg",
    alt: "It is lemon",
    count: 1,
  },
];

const Card = () => {
  const addItemToCart = (item) => (e) => {
    e.preventDefault();
    dispatch({ type: action.ADD, payload: item });
  };
  const dispatch = useDispatch();

  const cardItems = items.map((item, index) => (
    <div key={index} className="card__block">
      <img className="card__img" src={item.src} alt={item.alt} />
      <h4 className="card__title">{item.name}</h4>
      <p className="card__text">Price: ${item.price}</p>
      <a href="/" className="btn btn-blue" onClick={addItemToCart(item)}>
        Add to cart
      </a>
    </div>
  ));

  return <div className="card">{cardItems}</div>;
};

export default Card;
