import React, { useState } from "react";
import { useDispatch } from "react-redux";
import action from "../../reducers/actions";

import "./index.scss";

const ShopingItem = ({ item }) => {
  const [quantity, setQuantity] = useState(+item.count);
  const dispatch = useDispatch();

  const handleChange = (e) => {
    if (/^[\d.,:]*$/.test(e.target.value)) {
      dispatch({
        type: action.CHANGE_COUNT,
        payload: { id: item.id, count: e.target.value },
      });
      setQuantity(e.target.value);
    }
  };

  return (
    <div key={item.id} className="modal__element">
      <div className="element__name">{item.name}</div>
      <div className="element__price">${item.price * item.count}</div>
      <div className="element__input-wrap">
        <a
          className="element__decrement btn btn-blue"
          href="/"
          onClick={(e) => {
            e.preventDefault();
            setQuantity(item.count - 1);
            dispatch({
              type: action.CHANGE_COUNT,
              payload: { id: item.id, count: item.count - 1 },
            });
          }}
        >
          -
        </a>
        <input
          className="element__input"
          type="nubmer"
          // Если оставить quantity то при удалинии выше
          //  стоящего елемента будет обнуляться ниже стоящий
          value={item.count}
          onChange={handleChange}
        />
        <a
          className="element__increment btn btn-blue"
          href="/"
          onClick={(e) => {
            e.preventDefault();
            setQuantity(item.count + 1);
            dispatch({
              type: action.CHANGE_COUNT,
              payload: { id: item.id, count: item.count + 1 },
            });
          }}
        >
          +
        </a>
      </div>
      <a
        href="/"
        className="btn btn-red"
        onClick={(e) => {
          e.preventDefault();

          dispatch({
            type: action.DELETE_ITEM,
            payload: { id: item.id },
          });
        }}
      >
        X
      </a>
    </div>
  );
};

export default ShopingItem;
