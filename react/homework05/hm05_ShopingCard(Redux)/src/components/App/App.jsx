import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import action from "../../reducers/actions";
import Card from "../Card/Card";
import Header from "../Header/Header";
import ShopingCard from "../ShopingCard/ShopingCard";
// , { useState, useReducer }
import "./index.scss";

const App = () => {
  const orders = useSelector((state) => state);
  const [isModalVisible, setModalVisible] = useState(true);
  const dispatch = useDispatch();
  const quantity = orders.reduce((acc, item) => {
    if (item.count <= 0) {
      dispatch({ type: action.DELETE_ITEM, payload: { id: item.id } });
    }
    return (acc += +item.count);
  }, 0);

  return (
    <div className="App">
      <Header
        modal={isModalVisible}
        setModal={setModalVisible}
        quantity={quantity}
      />
      <h1>HELLO LEDIS AND PIDARS</h1>
      <main>
        <Card />
      </main>
      <ShopingCard
        modal={isModalVisible}
        setModal={setModalVisible}
        quantity={quantity}
      />
    </div>
  );
};

export default App;
