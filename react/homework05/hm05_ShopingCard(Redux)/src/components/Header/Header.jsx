import React from "react";
import { useDispatch } from "react-redux";
import action from "../../reducers/actions";

import "./index.scss";
// quantity
function Header({ modal, setModal, quantity }) {
  const handleShow = (e) => {
    e.preventDefault();
    setModal(!modal);
  };
  const dispatch = useDispatch();

  return (
    <header className="header">
      <div className="header__wrap">
        <a href="/" className="btn btn-green mr-2" onClick={handleShow}>
          Shoping KorZina ({quantity})
        </a>
        <a
          href="/"
          className="btn btn-red"
          onClick={(e) => {
            e.preventDefault();
            dispatch({ type: action.DELETE_ALL });
          }}
        >
          Clean KorZina
        </a>
      </div>
    </header>
  );
}

export default Header;
