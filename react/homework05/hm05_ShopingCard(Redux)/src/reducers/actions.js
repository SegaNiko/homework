const action = {
  ADD: "ADD",
  DELETE_ALL: "DELETE_ALL",
  DELETE_ITEM: "DELETE_ITEM",
  CHANGE_COUNT: "CHANGE_COUNT",
};

export default action;
