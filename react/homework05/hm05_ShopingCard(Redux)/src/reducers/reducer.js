import action from "./actions";
const initialState = [];

export const reducer = (orders = initialState, actions) => {
  switch (actions.type) {
    case action.ADD:
      return newOrder(orders, actions.payload);
    case action.DELETE_ITEM:
      return orders.filter((item) => item.id !== actions.payload.id);
    case action.DELETE_ALL:
      return [];
    case action.CHANGE_COUNT:
      return changeCount(orders, actions.payload);
    default:
      return orders;
  }
};

const changeCount = (orders, item) => {
  const { count } = item;
  const curIndex = orders.findIndex(({ id }) => id === item.id);
  const newOrders = [...orders];
  if (count === 0) {
    newOrders.filter(({ id }) => id !== item.id);
  }

  newOrders[curIndex].count = count;
  return newOrders;
};

const newOrder = (orders, item) => {
  const { id, name, price, count } = item;
  const curIndex = orders.findIndex(({ id }) => id === item.id);
  const newOrders = [...orders];

  if (curIndex === -1) {
    return [
      ...orders,
      {
        id,
        name,
        price,
        count,
      },
    ];
  } else {
    newOrders[curIndex].count += count;
    return newOrders;
  }
};
