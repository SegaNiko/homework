import React from "react";

import "./Footer.scss";
const Footer = () => {
  return (
    <footer className="footer">
      <p className="footer__copyright">
        Copyright by .
        <a
          href="https://www.instagram.com/p/B-fgsXJKt8I/?igshid=aw6byh1xrtgz"
          target="_blank"
          className="footer__name"
        >
          Sergiy Nikolaichuk
        </a>
      </p>
    </footer>
  );
};

export default Footer;
