import React from "react";
import { useSelector, useDispatch } from "react-redux";

import { showModal } from "../../store/actions";
import "./index.scss";

const Modal = () => {
  const result = useSelector((state) => state);
  const dispatch = useDispatch();

  return (
    <div className="modal__wrap">
      <div
        className={
          result.modal ? "modal__background" : "modal__hidden-background"
        }
        onClick={() => dispatch(showModal())}
      ></div>
      <div className={result.modal ? "modal" : "modal__hidden"}>
        <h2 className="modal__title">Your result</h2>
        <div className="modal__close" onClick={() => dispatch(showModal())}>
          ×
        </div>
        <div className="modal__body">
          <p className="modal__text">
            Reading Speed: {result.readingSpeed} (words in Min)
          </p>
          <p className="modal__text">
            Hours to read a book: {result.hoursToReadBook}
          </p>
        </div>
      </div>
    </div>
  );
};

export default Modal;
