import React, { useState } from "react";
import ACTIONS from "../App/actions";

import "./index.scss";

const ShopingItem = ({ item, key, dispatch }) => {
  const [quantity, setQuantity] = useState(item.count);

  const handlerChange = (e) => {
    if (/^[\d.,:]*$/.test(e.target.value)) {
      dispatch({
        type: ACTIONS.CHANGE_COUNT,
        payload: { id: item.id, count: e.target.value },
      });
      setQuantity(e.target.value);
    }
  };

  return (
    <div key={key} className="modal__element">
      <div className="element__name">{item.name}</div>
      <div className="element__price">${item.price * item.count}</div>
      <div className="element__input-wrap">
        <a
          className="element__decrement btn btn-blue"
          href="/"
          onClick={(e) => {
            e.preventDefault();
            setQuantity(item.count - 1);
            dispatch({
              type: ACTIONS.CHANGE_COUNT,
              payload: { id: item.id, count: item.count - 1 },
            });
          }}
        >
          -
        </a>
        <input
          className="element__input"
          type="nubmer"
          value={quantity}
          onChange={handlerChange}
        />
        <a
          className="element__increment btn btn-blue"
          href="/"
          onClick={(e) => {
            e.preventDefault();
            setQuantity(item.count + 1);
            dispatch({
              type: ACTIONS.CHANGE_COUNT,
              payload: { id: item.id, count: item.count + 1 },
            });
          }}
        >
          +
        </a>
      </div>
      <a
        href="/"
        className="btn btn-red"
        onClick={(e) => {
          e.preventDefault();
          dispatch({ type: ACTIONS.DELITE_ITEM, payload: { id: item.id } });
        }}
      >
        X
      </a>
    </div>
  );
};

export default ShopingItem;
