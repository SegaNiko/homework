import React, { useState, useReducer } from "react";

import reducer from "./reducer";

import Header from "../Header/Header";
import Card from "../Card/Card";
import ShopingCard from "../ShopingCard/ShopingCard";

import "./index.scss";
import ACTIONS from "./actions";

const App = () => {
  const [orders, dispatch] = useReducer(reducer, []);
  const [isModalVisible, setModalVisible] = useState(true);

  const quantity = orders.reduce((acc, item) => {
    if (item.count <= 0) {
      dispatch({ type: ACTIONS.DELITE_ITEM, payload: { id: item.id } });
    }
    return acc + +item.count;
  }, 0);

  return (
    <div className="App">
      <Header
        modal={isModalVisible}
        setModal={setModalVisible}
        dispatch={dispatch}
        quantity={quantity}
      />
      <main>
        <Card dispatch={dispatch} />
      </main>
      <ShopingCard
        modal={isModalVisible}
        orders={orders}
        setModal={setModalVisible}
        dispatch={dispatch}
        quantity={quantity}
      />
    </div>
  );
};

export default App;
