import ACTIONS from "./actions";

const reducer = (orders, actions) => {
  switch (actions.type) {
    case ACTIONS.ADD:
      return newOrder(orders, actions.payload);
    case ACTIONS.DELITE_ITEM:
      return orders.filter((item) => item.id !== actions.payload.id);
    case ACTIONS.CHANGE_COUNT:
      return conteChange(orders, actions.payload);
    case ACTIONS.DELETE_ALL:
      return [];
    default:
      return orders;
  }
};
const conteChange = (orders, item) => {
  const { count } = item;
  const curIndex = orders.findIndex(({ id }) => id === item.id);
  const newOrders = [...orders];

  newOrders[curIndex].count = count;
  return newOrders;
};
const newOrder = (orders, item) => {
  const { id, name, price, count } = item;
  const curIndex = orders.findIndex(({ id }) => id === item.id);
  const newOrders = [...orders];

  if (curIndex === -1) {
    return [
      ...orders,
      {
        id,
        name,
        price,
        count,
      },
    ];
  } else {
    newOrders[curIndex].count += count;
    return newOrders;
  }
};

export default reducer;
