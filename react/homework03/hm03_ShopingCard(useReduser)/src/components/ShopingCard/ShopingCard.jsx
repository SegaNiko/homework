import React from "react";

import ShopingItem from "../ShopingItem/ShopingItem";

import "./index.scss";

const ShopingCard = ({ modal, orders, setModal, dispatch, quantity }) => {
  const handleClose = (e) => {
    e.preventDefault();
    setModal(!modal);
  };
  const totalPrice = orders.length
    ? orders.reduce((acc, item) => {
        return Math.floor(acc + item.price * item.count);
      }, 0)
    : 0;

  const shopingElem = orders.map((item, index) => (
    <ShopingItem item={item} key={index} dispatch={dispatch} />
  ));
  return (
    <div className={modal ? "modal" : "modal-see"}>
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h3 className="modal-title">Shopping Card</h3>
            <div className="close" onClick={handleClose}>
              ×
            </div>
          </div>
          <div className="modal-body">
            {shopingElem}
            <div className="modal-info">
              <div className="element__total-price">
                Total price: ${totalPrice}
              </div>
              <div>Quantity: {quantity}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ShopingCard;
