import React from "react";

import ACTIONS from "../App/actions";

import "./index.scss";

function Header({ modal, setModal, dispatch, quantity }) {
  const handleShow = (e) => {
    e.preventDefault();
    setModal(!modal);
  };

  return (
    <header className="header">
      <div className="header__wrap">
        <a href="/" className="btn btn-green mr-2" onClick={handleShow}>
          Shoping KorZina ({quantity})
        </a>
        <a
          href="/"
          className="btn btn-red"
          onClick={(e) => {
            e.preventDefault();
            dispatch({ type: ACTIONS.DELETE_ALL });
          }}
        >
          Clean KorZina
        </a>
      </div>
    </header>
  );
}

export default Header;
