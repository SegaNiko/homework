import React, { useState } from "react";

import TableFormEdit from "../TableFormEdit/TableFormEdit";

import "./index.scss";

const Table = (props) => {
  const { allBooks, removeBook, updateBook } = props;
  const [edit, setEdit] = useState({
    id: null,
  });

  const editBook = (book) => {
    setEdit({
      id: null,
    });
    updateBook(book);

    console.log(edit);
  };
  const bookList = allBooks.map((item, index) => (
    <div key={item.id} className="table__item">
      <div>
        <p>{item.name}</p>
      </div>
      <div>
        <p>{item.author}</p>
      </div>
      <div>
        <p>{item.isbn}</p>
      </div>
      <div
        className="table__icon-wrap"
        onClick={() =>
          setEdit({
            id: item.id,
            name: item.name,
            author: item.author,
            index: index,
            isbn: item.isbn,
          })
        }
      >
        <a href="#" className="btn btb-green">
          <i className="fas fa-edit"></i>
        </a>
        <a href="#" className="btb-red btn" onClick={() => removeBook(item.id)}>
          X
        </a>
      </div>
    </div>
  ));

  if (edit.id) {
    console.log(edit);
    return (
      <div className="table">
        <div className="table__title">
          <div>
            <p>Title</p>
          </div>
          <div>
            <p>Author</p>
          </div>
          <div>
            <p>ISBN#</p>
          </div>
          <div>
            <p>pusto</p>
          </div>
        </div>
        <TableFormEdit editBook={editBook} edit={edit} />
      </div>
    );
  }

  return (
    <div className="table">
      <div className="table__title">
        <div>
          <p>Title</p>
        </div>
        <div>
          <p>Author</p>
        </div>
        <div>
          <p>ISBN#</p>
        </div>
        <div>
          <p>pusto</p>
        </div>
      </div>
      {bookList}
    </div>
  );
};

export default Table;
