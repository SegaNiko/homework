import React from "react";

import "./index.scss";

const Header = () => {
  return (
    <div className="header">
      <h1 className="header__title">
        <i className="fas fa-book-open text-primary"></i>
        <span className="header__subtitle">Book</span> List
      </h1>
      <p className="header__text">
        Add your book information to store it in database.
      </p>
    </div>
  );
};

export default Header;
