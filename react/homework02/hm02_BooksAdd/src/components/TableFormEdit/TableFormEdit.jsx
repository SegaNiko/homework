import React, { useState } from "react";

import "./index.scss";

const TableFormEdit = (props) => {
  const { editBook, edit } = props;
  const [title, setTitle] = useState("");
  const [author, setAuthor] = useState("");
  const [isbn, setIsbn] = useState("");
  // хендлеры для формы !!!Хз как сделать один обработчик на всю форму!!!!!
  // Мне не нравится этот код

  const hendleChangeTitle = (e) => {
    // Проверки остаил на потом
    setTitle(e.target.value);
  };
  const hendleChangeAuthor = (e) => {
    setAuthor(e.target.value);
  };
  const hendleChangeIsbn = (e) => {
    setIsbn(e.target.value);
  };
  const handleSubmit = (e) => {
    // Убираем дефолтную перезагрузку по клике на сабмит
    e.preventDefault();

    //  Передача форми по клику на сабмит в
    props.editBook({
      id: edit.id,
      name: title,
      author: author,
      isbn: isbn,
      index: edit.index,
    });

    // очищение импутов
    setTitle("");
    setAuthor("");
    setIsbn("");
  };
  return (
    <div>
      <form className="edit" onSubmit={handleSubmit}>
        <div className="edit__item">
          <input
            type="text"
            className="edit__input"
            value={title}
            onChange={hendleChangeTitle}
            placeholder={edit.name}
          ></input>
        </div>
        <div className="edit__item">
          <input
            type="text"
            className="edit__input"
            value={author}
            onChange={hendleChangeAuthor}
            placeholder={edit.author}
          ></input>
        </div>
        <div className="edit__item">
          <input
            type="text"
            className="edit__input"
            value={isbn}
            onChange={hendleChangeIsbn}
            placeholder={edit.isbn}
          ></input>
        </div>
        <div className="edit__item">
          <input
            type="submit"
            value="Update Book"
            className="btn btn-blue"
          ></input>
        </div>
      </form>
    </div>
  );
};

export default TableFormEdit;
