import React, { useState } from "react";

import Table from "../Table/Table";
import Header from "../Header/Header";

import "./index.scss";
import TableForm from "../TableForm/TableForm";

const App = () => {
  const [books, setBook] = useState([]);

  const addBook = (book) => {
    const newBooks = [book, ...books];
    setBook(newBooks);
  };

  const removeBook = (id) => {
    const removeArr = [...books].filter((book) => book.id !== id);
    setBook(removeArr);
  };
  const updateBook = (changeBook) => {
    console.log(changeBook);
    // ((((интересны другие способы ревлизация доного примера))))
    const newBooks = books.splice(changeBook.index, 1, changeBook);
    setBook(books);
  };
  return (
    <div className="app">
      <div className="container">
        <Header />
        <TableForm onSubmit={addBook} />

        <h3 className="counter">Всего книг: {books.length} </h3>
        <main className="main">
          <Table
            allBooks={books}
            removeBook={removeBook}
            updateBook={updateBook}
          />
        </main>
      </div>
    </div>
  );
};

export default App;
