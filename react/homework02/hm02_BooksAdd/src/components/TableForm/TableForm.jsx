import React, { useState } from "react";

import "./index.scss";

const TableForm = (props) => {
  const [title, setTitle] = useState("");
  const [author, setAuthor] = useState("");
  const [isbn, setIsbn] = useState("");
  // хендлеры для формы !!!Хз как сделать один обработчик на всю форму!!!!!
  // Мне не нравится этот код
  const hendleChangeTitle = (e) => {
    setTitle(e.target.value);
    console.log(title);
  };
  const hendleChangeAuthor = (e) => {
    setAuthor(e.target.value);
  };
  const hendleChangeIsbn = (e) => {
    setIsbn(e.target.value);
  };
  const handleSubmit = (e) => {
    // Убираем дефолтную перезагрузку по клике на сабмит
    e.preventDefault();

    //  Передача форми по клику на сабмит в
    props.onSubmit({
      id: Math.floor(Math.random() * 10000),
      name: title,
      author: author,
      isbn: isbn,
    });
    // очищение импутов
    setTitle("");
    setAuthor("");
    setIsbn("");
  };
  return (
    <div className="container">
      <form className="form" onSubmit={handleSubmit}>
        <div className="form__item">
          <label>Title</label>
          <input
            type="text"
            name="book-name"
            className="form__input"
            value={title}
            onChange={hendleChangeTitle}
          />
        </div>
        <div className="form__item">
          <label>Author</label>
          <input
            type="text"
            name="book-author"
            className="form__input"
            value={author}
            onChange={hendleChangeAuthor}
          />
        </div>
        <div className="form__item">
          <label>ISBN#</label>
          <input
            type="text"
            name="book-isbn"
            className="form__input"
            value={isbn}
            onChange={hendleChangeIsbn}
          />
        </div>
        <input type="submit" value="Add Book" className="btn btn-blue" />
      </form>
    </div>
  );
};

export default TableForm;
