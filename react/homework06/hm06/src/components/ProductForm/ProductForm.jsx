import React, { useState } from "react";
import { Formik, Field, Form, ErrorMessage } from "formik";
import { render } from "react-dom";
import * as Yup from "yup";
// import InputMask from "react-input-mask"; was tested InputMask
import MaskedInput from "react-text-mask";

import "./index.scss";

const phoneNumberMask = [
  "+",
  "(",
  /[1-9]/,
  /\d/,
  /\d/,
  ")",
  " ",
  /\d/,
  /\d/,
  " ",
  /\d/,
  /\d/,
  /\d/,
  " ",
  /\d/,
  /\d/,
  " ",
  /\d/,
  /\d/,
];

const ProductForm = () => {
  // Громосткость кода будет исправлятся после решения всех задач

  const initialValues = {
    firstName: "",
    lastName: "",
    email: "",
    surName: "",
    phone: "",
    city: "Kiev",
    deliveryMethod: "",
    comments: "",
    subscribe: false,
  };
  const [deliveryType, setDeliveryType] = useState();

  const validationSchema = Yup.object({
    firstName: Yup.string()
      .max(15, "Must be 15 characters or less")
      .required("Required"),
    surName: Yup.string()
      .max(20, "Must be 20 characters or less")
      .required("Required"),
    lastName: Yup.string()
      .max(20, "Must be 20 characters or less")
      .required("Required"),
    email: Yup.string().email("Invalid email address").required("Required"),
    phone: Yup.string().min(16, "lodx").required("Required"), //проблема с валидациий
  });
  const onSubmit = (values, { setSubmitting }) => {
    setTimeout(() => {
      alert(JSON.stringify(values, null, 2));
      setSubmitting(false);
    }, 400);
  };
  const optionsCity = [
    {
      value: "Kiev",
      text: "Kiev",
    },
    {
      value: "Odessa",
      text: "Odessa",
    },
    {
      value: "Lutsk",
      text: "Lutsk",
    },
    {
      value: "Amerika",
      text: "Amerika",
    },
  ];
  const optionsDelivery = [
    {
      value: "NovaPochta",
      text: "NovaPochta",
    },
    {
      value: "UkrPochta",
      text: "UkrPochta",
    },
  ];

  const categoryOptionsCity = optionsCity.map(({ value, text }) => (
    <option key={text} value={value}>
      {text}
    </option>
  ));
  const categoryOptionsDelivery = optionsDelivery.map(({ value, text }) => (
    <option key={text} value={value}>
      {text}
    </option>
  ));

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={onSubmit}
    >
      <Form className="form">
        <div className="form_blok">
          <label htmlFor="firstName">First Name: </label>
          <Field className="input" name="firstName" type="text" />
          <ErrorMessage component="div" name="firstName" />
        </div>
        <div className="form_blok">
          <label htmlFor="surName">Surname: </label>
          <Field className="input" name="surName" type="text" />
          <ErrorMessage component="div" name="surName" />
        </div>
        <div className="form_blok">
          <label htmlFor="lastName">Last Name: </label>
          <Field className="input" name="lastName" type="text" />
          <ErrorMessage component="div" name="lastName" />
        </div>
        <div className="form_blok">
          <label htmlFor="email">Email Address: </label>
          <Field className="input" name="email" type="email" />
          <ErrorMessage component="div" name="email" />
        </div>

        <div className="form_blok">
          <label htmlFor="phone">Phone: </label>
          <Field name="phone">
            {({ field }) => (
              <MaskedInput
                {...field}
                className="input"
                mask={phoneNumberMask}
                id="phone"
                placeholder="Enter your phone number"
                type="text"
              />
            )}
          </Field>
          <ErrorMessage component="div" name="phone" />
        </div>
        <div className="form_blok">
          <label htmlFor="city">City: </label>
          <Field as="select" name="city">
            {categoryOptionsCity}
          </Field>
        </div>
        <div className="form_blok">
          <label htmlFor="deliveryMethod">Delivery method: </label>
          <Field as="select" name="deliveryMethod">
            {categoryOptionsDelivery}
          </Field>
        </div>

        {/* 
        Проблемы с отрисовкою елемента так как 
        начальное состояние меняется после сабмита
         */}
        {initialValues.deliveryMethod === "UkrPochta" ? (
          <div> UkrPochta</div>
        ) : (
          <div>NovaPochta</div>
        )}
        <div className="form_blok">
          <Field type="checkbox" name="subscribe" />
          <span htmlFor="city"> Subscribe to newsletter</span>
        </div>
        <div className="form_blok">
          <label htmlFor="city">Comments on the order: </label>
          <Field
            className="textarea"
            name="coments"
            as="textarea"
            placeHolder="Post a message.."
            cols="50"
            wrap="off"
            rows={5}
          />
        </div>

        <button className="submit" type="submit">
          Оплатить
        </button>
      </Form>
    </Formik>
  );
};

export default ProductForm;
