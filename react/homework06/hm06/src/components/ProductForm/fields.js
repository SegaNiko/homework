export const fields = {
  email: {
      type: "email",
      name: "email",
      placeholder: "email"
  },
  password: {
      type: "password",
      name: "password",
      placeholder: "password"
  },
  category: {
      name: "category",
      options:  [{
          value: "laptop",
          text: "laptop"
      },{
          value: "tablet",
          text: "tablet"
      },{
          value: "phone",
          text: "phone"
      }]
  },
  submit: {
      type: "submit"
  }
};
