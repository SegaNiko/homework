import React from "react";

import "./App.scss";
import ProductForm from "../ProductForm/ProductForm";
// import ProductFormuseFormik from "../ProductFormuseFormik/ProductFormuseFormik";

function App() {
  return (
    <div className="App">
      <div className="container">
        <h1 className="title">Form regestrarion</h1>
        <ProductForm />
        {/* <ProductFormuseFormik /> */}
      </div>
    </div>
  );
}

export default App;
