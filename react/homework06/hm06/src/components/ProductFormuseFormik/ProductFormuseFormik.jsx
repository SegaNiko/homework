import React from "react";
import { Field, Form, ErrorMessage, useFormik } from "formik";
import * as Yup from "yup";
import InputMask from "react-input-mask";

import "./index.scss";

const ProductFormuseFormik = () => {
  const formik = useFormik({
    initialValues: {
      firstName: "",
      surName: "",
      lastName: "",
      email: "",
      phone: "",
      city: "Kiev",
      toggle: false,
    },
    validationSchema: Yup.object({
      firstName: Yup.string()
        .max(15, "Must be 15 characters or less")
        .required("Required"),
      lastName: Yup.string()
        .max(20, "Must be 20 characters or less")
        .required("Required"),
      email: Yup.string().email("Invalid email address").required("Required"),
    }),
    onSubmit: (values) => {
      alert(JSON.stringify(values, null, 2));
    },
  });

  const options = [
    {
      value: "Kiev",
      text: "Kiev",
    },
    {
      value: "Odessa",
      text: "Odessa",
    },
    {
      value: "Lutsk",
      text: "Lutsk",
    },
    {
      value: "Amerika",
      text: "Amerika",
    },
  ];

  const categoryOptions = options.map(({ value, text }) => (
    <option key={text} value={value}>
      {text}
    </option>
  ));
  const phoneMask = {
    mask: "99/99/9999",
    maskChar: "_",
    alwaysShowMask: false,
    formatChars: {
      9: "[0-9]",
      a: "[A-Za-z]",
      "*": "[A-Za-z0-9]",
    },
    permanents: [2, 5], // permanents is an array of indexes of the non-editable characters in the mask
  };

  return (
    <form onSubmit={formik.handleSubmit}>
      <div className="form_blok">
        <label htmlFor="firstName">First Name</label>
        <input
          className="input"
          type="text"
          {...formik.getFieldProps("firstName")}
        />
        {formik.touched.firstName && formik.errors.firstName ? (
          <div>{formik.errors.firstName}</div>
        ) : null}
      </div>
      <div className="form_blok">
        <label htmlFor="surName">Surname: </label>
        <input
          className="input"
          type="surName"
          {...formik.getFieldProps("surName")}
        />
        {formik.touched.surName && formik.errors.surName ? (
          <div>{formik.errors.surName}</div>
        ) : null}
      </div>
      <div className="form_blok">
        <label htmlFor="lastName">Last Name</label>
        <input
          className="input"
          type="text"
          {...formik.getFieldProps("lastName")}
        />
        {formik.touched.lastName && formik.errors.lastName ? (
          <div>{formik.errors.lastName}</div>
        ) : null}
      </div>
      <div className="form_blok">
        <label htmlFor="email">Email Address</label>
        <input
          className="input"
          type="email"
          {...formik.getFieldProps("email")}
        />
        {formik.touched.email && formik.errors.email ? (
          <div>{formik.errors.email}</div>
        ) : null}
      </div>
      <div className="form_blok">
        <label htmlFor="surName">Surname: </label>
        {console.log({ ...formik.getFieldProps("email") })}
        {console.log(InputMask)}

        <InputMask
          mask="+389 99 999 99 99"
          value={phoneMask}
          onChange={formik.handleChange}
          name="phone"
        >
          {(inputProps) => (
            <input
              {...inputProps}
              type="phone"
              disableUnderline
              onChange={formik.handleChange}
            />
          )}
        </InputMask>
        <input
          className="input"
          type="surName"
          {...formik.getFieldProps("surName")}
        />
        {formik.touched.surName && formik.errors.surName ? (
          <div>{formik.errors.surName}</div>
        ) : null}
      </div>
      <button type="submit">Submit</button>
    </form>
  );
};

export default ProductFormuseFormik;
