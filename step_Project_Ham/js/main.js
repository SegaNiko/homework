window.addEventListener("DOMContentLoaded", () => {
  sliderActiv();
  tabs(".tabs", ".tab", ".tab_item", "active");
  tabs(
    ".review__slider",
    ".review__block",
    ".review__slider-content",
    "active"
  );
  tabs(".works__list", ".works__link", ".works__content", "active__works");
});

const tabs = (
  headerSelector,
  tabSelector,
  contentSelector,
  activeClass,
  display = "flex",
  count = 8
) => {
  const header = document.querySelector(headerSelector),
    tab = document.querySelectorAll(tabSelector),
    content = document.querySelectorAll(contentSelector);
  console.log("TAB", tab, "CONTENT", content, "HEADER", header);
  console.log(typeof tab);

  function hideTabContent() {
    content.forEach((item) => {
      item.style.display = "none";
    });

    tab.forEach((item) => {
      item.classList.remove(activeClass);
    });
  }

  function showTabContent(num = 0) {
    content[num].style.display = display;
    tab[num].classList.add(activeClass);
    if (content[num].getAttribute("data-name")) {
      const arr = content[num];
      console.log(typeof arr);

      // arr.forEach(item => {
      //   console.log(item )
      // })

      return;
    }
  }
  hideTabContent();
  showTabContent();

  header.addEventListener("click", (e) => {
    e.preventDefault();
    const target = e.target;
    if (
      target &&
      (target.classList.contains(tabSelector.replace(/\./, "")) ||
        target.parentNode.classList.contains(tabSelector.replace(/\./, "")))
    ) {
      tab.forEach((item, i) => {
        if (target == item || target.parentNode == item) {
          hideTabContent();
          showTabContent(i);
        }
      });
    }
  });
};

const sliderActiv = () => {
  $(".review__slider").slick({
    accessibility: false,
    infinite: false,
    centerMode: true,
    centerPadding: "100px",
    slidesToShow: 5,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: "40px",
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: "40px",
          slidesToShow: 1,
        },
      },
    ],
  });
};

const showMoreContent = (
  btnLoadMore,
  contentToShow,
  countImg = 12,
  display = "flex"
) => {
  const content = document.querySelectorAll(contentToShow);
  const btn = document.querySelector(btnLoadMore);
};
